<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Swiper demo</title>
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"
    />
    <!-- Link Swiper's CSS -->
    <link
      rel="stylesheet"
      href="https://unpkg.com/swiper/swiper-bundle.min.css"
    />
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

    <!-- Demo styles -->
    <style>
    html,
    body {
        position: relative;
        height: 100%;
    }

    body {
        background: #eee;
        font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
        font-size: 14px;
        color: #000;
        margin: 0;
        padding: 0;
        width: 100%;
    }

    .swiper-container {
        width: 100%;
        height: 100%;
    }

    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;

        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }

    .swiper-slide img {
        display: block;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    .swiper-container-horizontal>.swiper-pagination-bullets, .swiper-pagination-custom, .swiper-pagination-fraction {
        left: 70px;
        bottom: 40px;
    }
    .swiper-pagination {
        text-align: left;
    }
    .swiper-pagination-bullet-active {
        background-color: grey;
    }
    .swiper-pagination-bullet {
        background: white;
    }
    .swiper-button-next, .swiper-button-prev {
        color: white;
    }
    .swiper2 {
        background-color: #ffffff00 !important;
        width: 280px !important;
        height: 320px;
        border-radius: 4px;
        box-shadow: inset 0px 0px 0px 1px $neu-02;
        position: relative;
        transition: all 0.4s ease;

        &:before {
            content: '';
            position: absolute;
            width: 100%;
            height: 100%;
            box-shadow: 0px 20px 40px -20px #EAEAEA;
            top: 0px;
            z-index: -1;
            transition: all 0.4s ease;
        }
    }
    </style>
  </head>

  <body class="">
    <!-- Swiper -->
    <div class="container pt-2">
        <div class="container-fluid bg-light py-2 rounded">

            <div class="row">
                <div class="col-md-12" style="height: 600px;">
                    <div class="swiper-container mySwiper" style="border-radius: 25px;">
                        <div class="swiper-wrapper">
                            @foreach($data as $movie)
                            
                                @if($movie->rank >= 1 && $movie->rank <= 10)
                                    <div class="swiper-slide rounded" style="background: url('{{$movie->image}}'); background-repeat: no-repeat; background-size:cover;">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-1 pt-5 text-white" style="text-align: left;">
                                                </div>
                                                <div class="col-md-5 pt-5 text-white" style="text-align: left;">
                                                    <h1 class="swiper-no-swiping"> </h1>
                                                    <p class="swiper-no-swiping">{{$movie->fullTitle}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="swiper-button-next" style="right: 50px;"></div>
                        <div class="swiper-button-prev" style="left: 50px;"></div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
            
            <div class="row pt-2">
                <div class="col-md-12 pt-3">
                    <h3><b>Related</b></h3>
                </div>
                <div class="col-md-12" style="height: 400px;">
                    <div class="swiper-container mySwiper2">
                        <div class="swiper-wrapper" style="transform: translate3d(100px, 0px, 0px); transition-duration: 0ms;">

                        @foreach($data as $movie)             
                            @if($movie->rank >= 11 && $movie->rank <= 20)
                                <div class="swiper-slide swiper2">
                                    <div class="card h-100 w-100 border-0" style="background-color: #fff0;">
                                        <img src="{{ $movie->image }}" class="card-img-top shadow" alt="..." style="border-radius: 25px;">
                                        <div class="card-body" style="background-color: #fff0;">
                                            <h5 class="card-title swiper-no-swiping" style="text-align: left;">{{$movie->title}}</h5>
                                            <!-- <p class="card-text swiper-no-swiping" style="font-size:12px;">Ini deskripsi gaes</p> -->
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach


                        </div>
                    </div>
                </div>
            </div>
        </div>
        

    </div>


    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
      var swiper = new Swiper(".mySwiper", {
        loopFillGroupWithBlank: true,
        pagination: {
        el: '.swiper-pagination',
        clickable: true,
        },
        autoplay: {
        delay: 5000,
        },
        navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
        },
        debugger: true,
        loop: true,
      });
      var swiper2 = new Swiper(".mySwiper2", {
        centeredSlides: false,
        slidesPerView: 4,
        spaceBetween: 30,
        pagination: {
            clickable: true,
        },
      });
     
    </script>
  </body>
</html>
