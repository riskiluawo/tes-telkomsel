<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;

class HomeController extends Controller
{

    public function apimovie()
    {
        $options = array(
            'http' => array(
                'method'  => 'GET',
                'header' =>  "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
            )
        );
        $url = 'https://imdb-api.com/en/API/Top250Movies/k_quzyc85y';
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $response = json_decode($result);
        return $response->items;
    }

    public function tes(Request $request)
    {
        $data = $this->apimovie();
        // dd($data-);

        return view('welcome', compact('data'));
    }
}
